# Watson BotOnes
Watson Assistant integration with IoT devices in order to provide an interactive chatbot.

## Functionality
The purpose of this project is to provide a service to the clients. The idea is to simulate that a client is talking to a bartender or a butler, for example, in a hotel. This is achieved by the Watson Assistant chatbot API.
Also, the chatbot has to be able to control real devices like speakers, lights, etc. This is achieved by the communication between IoT devices and the web application.
The web application integrates the chatbot with the multiple devices, and serves a client side user interface to interact with all its capabilites.
